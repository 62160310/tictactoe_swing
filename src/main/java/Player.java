
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Bunny0_
 */
public class Player implements Serializable{

    char player;
    int win;
    int lose;
    int draw;

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    public char getPlayer() {
        return player;
    }

    public Player(char player) {
        this.player = player;
    }
    public void win() {
        win++;
    }
    
    public void lose() {
        lose++;
    }
    
    public void draw() {
        draw++;
    }
    public void setPlayer(char player){
        this.player = player;
    }

   
    
    

    

}
